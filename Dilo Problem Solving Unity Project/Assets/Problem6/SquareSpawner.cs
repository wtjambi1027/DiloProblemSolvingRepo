﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareSpawner : MonoBehaviour
{
    public GameObject square;

    // Start is called before the first frame update
    void Start()
    {
        int amount = Random.Range(6, 10);

        while(amount > 0)
        {
            Vector2 ranPos = new Vector2(Random.Range(-8, 8), Random.Range(-4, 4));

            GameObject newSquare = Instantiate(square, transform);
            square.transform.position = ranPos;

            amount--;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
