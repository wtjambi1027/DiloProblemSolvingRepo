﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleController6 : MonoBehaviour
{
    private Rigidbody2D rb2D;

    public float speed = 100;

    private Vector2 direction;
    private Vector2 velocity;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        //direction.x = Input.GetAxis("Horizontal");
        //direction.y = Input.GetAxis("Vertical");

        //velocity = direction.normalized * speed;

        direction = Vector2.zero;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            direction.x = -1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            direction.x = 1;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            direction.y = 1;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            direction.y = -1;
        }
    }

    private void FixedUpdate()
    {
        velocity = direction.normalized * speed;
        rb2D.velocity = velocity;
    }
}
