﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleController2 : MonoBehaviour
{
    private Rigidbody2D rb2D;

    public float speed = 100;

    private Vector2 velocity;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        velocity = new Vector2(1, 1) * speed;
    }

    void FixedUpdate()
    {
        rb2D.velocity = velocity;
    }
}
