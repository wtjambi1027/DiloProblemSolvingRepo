﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleController5 : MonoBehaviour
{
    private Rigidbody2D rb2D;

    public float speed = 100;

    private Vector2 direction;
    private Vector2 velocity;
    private bool isMoving = false;
    private Vector2 target;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) && !isMoving)
        {
            isMoving = true;
            target = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        }
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            direction = target - rb2D.position;
            velocity = direction.normalized * speed;
            rb2D.velocity = velocity;
        }

        if ((rb2D.position - target).magnitude <= 0.1f)
        {
            isMoving = false;
            rb2D.velocity = Vector2.zero;
        }
    }
}
